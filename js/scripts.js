const menuHamburguer = document.querySelector(`.header-menu-hamburguer`)
const menuItens = document.querySelector(`.header-menu-mobile`)
const serviceBoxes = document.querySelectorAll(`.service-box`)
const coverContainer = document.querySelectorAll(`.cover-container`)
const coverList = document.querySelectorAll(`.cover-list`)
const coverTitle = document.querySelectorAll(`.cover-box`)
const reportCard = document.querySelectorAll(`.report-card`)
const report = document.querySelector(`.report`)

menuHamburguer.onclick = showMenu

for (item in serviceBoxes) {
    serviceBoxes[item].onmouseover  = showServiceBox
    serviceBoxes[item].onmouseout = hiddeServiceBox
}

for (item in coverContainer) {
    coverContainer[item].onmouseover = showCoversList
    coverContainer[item].onmouseout = hiddenCoverList
}

for (item in reportCard) {
    reportCard[item].onmouseover = focusReport;
    reportCard[item].onmouseout = unfocusReport;
}


