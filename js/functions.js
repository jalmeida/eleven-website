const showMenu = _ => {
    if (menuItens.style.display === `block`) {
        menuItens.style.display = `none`
    } else {
        menuItens.style.display = `block`
    }
}

function showServiceBox() {
    this.style.top = `0`
    this.style.transitionDuration = `1s`;
    // this.style.animation = `showServiceBox 2s 0.5s`
}

function hiddeServiceBox() {
    this.style.top = `250px`
    // this.style.animation = `hiddeServiceBox 2s`
}

function showCoversList() {
    this.children[0].style.animation = `flipBox 2s`;
    toggle(this.children[0].children[0]);
    toggle(this.children[0].children[1]);
}

function hiddenCoverList() {
    // this.style.transform = `rotateY(360deg)`
    this.children[0].style.animation = `unflipBox 2s`;
    toggle(this.children[0].children[0]);
    toggle(this.children[0].children[1]);
}

function toggle(element) {
    return setTimeout(function () {
        console.log(element);
        element.classList.toggle('visible');
        element.classList.toggle('invisible');
    }, 1000);
}

function focusReport() {
    this.style.transform = `scale(1.05, 1.05)`
    this.style.transitionDuration = `1s`
    
}

function unfocusReport() {
    this.style.transform = `scale(1, 1)`
}